# Logisim Projects

These are a variety of projects built in Logisim to help supplement understanding of logical thinking. The outline/expectations were given by Ian Finlayson.

## CPU Part 1 / 2

A two part project designed to build a cpu inside Logisim that could handle ARM assembly instructions. The CPU itself was split into several different circuits that reconnect in the main.

It's comprised of a series of registries to build our memory, logic gates for the ALU operations, and a fetcher for retrieval.
The program also incorporated higher level circuits, such as Multiplexers, Decoders, Screens, and large binary operators.

## Flip Flop

A demonstration of how "memory" is preserved by using flip flops. 

## Gates

A simple logisim circuit to simulate a digital number screen.

## CheapoToe

Tic Tac Toe built within Logisim. It uses 0's for player 1, and 1's for player 2. When the buttons are pressed, a large logic circuit determines if the player has won with the current board state, then lights up if they have.

## DateChecker

Uses logic circuits to determine if a day/month combination is valid. For example, April 31st would not be considered a valid day, as there is no 31st day in April. Days and Months are represented binarily.